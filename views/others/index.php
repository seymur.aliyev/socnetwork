<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'My Yii Application';
?>
<div class="row">
    <div class="col-md-9">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>

    <div class="row cover-image" style="height: 315px;border: 1px solid #d2d1d1;">
        <a href="<?=Url::to(['common/photo','id'=> $current_cover_image['id']]);?>">
            <img style='width: 100%;max-height: 100%;' src="<?=(Yii::getAlias('@web').'/web/uploads/cover_image/'.($current_cover_image['hash_name'] ? $current_cover_image['hash_name'] : 'no-image.png'));?>" alt=""/>
        </a>
        <div class="col-md-12" style="padding: 10px;position: absolute;top: 0px;">
            <div class="block-profile-image" style="display: none;">
                <div class="panel panel-default" style="height: 100%;">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <h5>Update Profile Image</h5>
                                </div>
                                <div class="col-md-6">
                                    <a href="#" id="close-profile-image-block">Close</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12" style="border: 1px solid black;padding-top: 20px;">
                            <?= $form->field($userPhotoModel, 'profile_image', [
                            ])->fileInput()
                                ->label('<p style="cursor: pointer;"><i id="profile-image-icon" style="font-size: 20px;opacity:0.9;" class="fa fa-camera-retro"></i> Upload profile</p>');
                            ?>
                        </div>
                        <div class="col-md-12" style="padding-left: 0;padding-right: 0;">
                            <?php
                            $byType = [];
                            foreach ($all_photos as $k => $data) {
                                $byType[$data['type']][] = $data;
                            }
                            ?>
                            <?php foreach ($byType as $k => $images) {?>
                                <div class="col-md-12">
                                    <h4><?=$k?></h4>
                                    <br/>
                                    <?php foreach ($images as $image) {?>
                                        <div class="col-md-3">
                                            <input class="hideit" type='radio' value="<?=$image['id'];?>" name='chooseProfileImage' id="radio<?=$image['id'];?>"/>
                                            <label for="radio<?=$image['id'];?>">
                                                <a type="button" class="btn btn-default">
                                                    <img style="height: 100%;width: 100%;" src="<?=Yii::getAlias('@web').'/web/uploads/'.$k.'/'.$image['hash_name'];?>" alt="">
                                                </a>
                                            </label>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <style>
                            .hideit {
                                visibility: hidden;
                            }

                            .diplomaType li label {
                                cursor: pointer;
                            }

                            .hideit:checked + label a {
                                background: #c7e3e6;
                            }
                        </style>
                        <input type="submit" value="Use this image" class="btn btn-primary"/>
                    </div>
                </div>
            </div>

           <!-- <div class="col-md-4" id="coverUpload" style="padding: 10px;position: absolute;top: 0px;padding-left: 25px;">
                <?/*= $form->field($userPhotoModel, 'cover_image', [
                ])->fileInput()
                    ->label('<i id="cover-image-icon" style="color:#e4ddda;font-size: 25px;opacity:0.9;cursor: pointer;" class="fa fa-camera-retro"></i>
                        <p id="hidden-cover-image-label-text" style="display: none;"></p>')
                */?>
            </div>-->
        </div>

        <div class="col-md-12" style="position: absolute;top: 130px;">
            <div class="col-md-3 profile-image">
                <div style="height: 168px;width:168px;">
                    <img src="<?=(Yii::getAlias('@web').'/web/uploads/profile_image/'.($current_profile_image ? $current_profile_image : 'no-image.png'));?>" alt="" style="
                            max-height: 100%;width:100%;
                            border-radius: 5px 20px 5px;border: 2px solid white;
                                ">
                </div>
                <!--<div class="col-md-12" style="bottom: 46px;height: 45px;background: #504a4a;">
                    <a class='btn btn-default' id="profile_image_box" >Upload Image</a>
                </div>-->

            </div>
            <div class="col-md-3 profile-name" style="padding-left: 0;">
                <h1 style="color: white;">
                    <a style="text-decoration: none;color:white;"
                       href="<?= Url::toRoute(['/'.Yii::$app->user->identity['name']]);?>"><?=Yii::$app->user->identity['name'];?>
                    </a>
                </h1>
            </div>
        </div>
    </div>

    <div class="row" style="padding-left: 0;padding-right: 0;margin-top: 15px;">
        <div class="col-md-5" style="padding-left: 0;">
            <div class="intro" >
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-12">
                                <a style="" href="<?=Url::toRoute([Yii::$app->user->identity['name'].'/about']);?>">
                                    <i class="fa fa-user"></i> About
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php foreach ($allWorkPlaces as $workPlace) {?>
                            <div class="col-md-12">
                                <i class="fa fa-briefcase"></i> <?=$workPlace['position'];?> at <?=$workPlace['company'];?>
                            </div>
                        <?php } ?>
                        <?php foreach ($allUniversities as $university) {?>
                            <div class="col-md-12">
                                <i class="fa fa-graduation-cap"></i> studied at <?=$university['name'];?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="friends">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-12">
                                <a style="" href="<?=Url::toRoute(['user/friends','id'=> Yii::$app->request->get('id')]);?>">
                                    <i class="fa fa-users"></i> Friends
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php foreach ($friends as $friend) {?>
                            <div class="col-md-4">
                                <img src="<?=(Yii::getAlias('@web').'/web/uploads/profile_image/'.($friend['hash_name'] ? $friend['hash_name'] : 'no-image.png'));?>" alt=""
                                     style="border: 2px solid white;width: 100%;height: 100%;">
                                <a style="position: absolute;bottom: 0;left: 20px;color:white;"
                                   href="<?=Url::toRoute(['profile/profile','id' => $friend['id'] ]); ?>">
                                    <?=$friend['name'];?>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="photos">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-12">
                                <a style="" href="<?=Url::toRoute([Yii::$app->user->identity['name'].'/photo-albums']);?>">
                                    <i class="fa fa-users"></i> Photos
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php if(!empty($user_photos)) {
                            foreach ($user_photos as $k => $photo) { ?>
                                <div class="col-md-3">
                                    <img style="max-height: 100%;width: 100%;" src="<?=Yii::getAlias('@web').'/web/uploads/profile_image/'.$photo['hash_name'];?>" alt="">
                                </div>
                        <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-7" style="padding-right: 0;">
            <div class="user-timeLine">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row" style="height: 20px;">
                            <div class="col-md-3" style="height: 100%;padding-left:25px;border-right: 1px solid black;">
                                <?= $form->field($userPhotoModel, 'other_image', [
                                ])->fileInput()
                                    ->label('
                                    <div class="col-md-12"  style="cursor: pointer;padding-left:0;">
                                    <div class="col-md-6" style="padding-left: 0;padding-right: 0;text-align: center;">
                                        <i id="content-image-icon" style="font-size: 20px;opacity:0.9;cursor: pointer;" 
                                        class="fa fa-camera-retro"></i>
                                        </div>
                                        <div class="col-md-6" style="padding-left: 0;">
                                        <p>Photo</p>
                                        </div>
                                    </div>
                                 ');
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                            <div class="col-md-2">
                                <img class="image-circle" src="<?=(Yii::getAlias('@web').'/web/uploads/profile_image/'.($current_profile_image ? $current_profile_image : 'no-image.png'));?>" alt=""
                                     style="border-radius: 50%;width:40px;height:40px;"/>
                            </div>
                            <div class="col-md-10" style="padding-left: 0;">
                                <textarea style="resize:none;width: 100%;height: 90px;border: none;outline:none;"
                                          placeholder="What is in your mind?" name="" id=""></textarea>
                            </div>
                    </div>

                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-8" style="float: right;padding-right: 0;">

                                <div class="col-md-4" style="float: right;">
                                    <button class="btn btn-primary" style="float: right;">POST</button>
                                </div>

                                <div class="col-md-6" style="float: right;">
                                    <select class="form-control" name="" id="">
                                        <option value="">Only me</option>
                                        <option value="">Public</option>
                                        <option value="">Friends</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="user-content">
                <div class="post">
                    <div class="panel panel-default">
                        <div class="panel-heading">Content</div>
                        <div class="panel-body">
                            Content body
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-md-4" style="text-align: center;">
                                    <a href="#">
                                        <i class="fa fa-thumbs-o-up"></i> Like
                                    </a>

                                </div>
                                <div class="col-md-4" style="text-align: center;">
                                    <a href="#">
                                        <i class="fa fa-comment-o"></i> Comment
                                    </a>
                                </div>
                                <div class="col-md-4" style="text-align: center;">
                                    <a style="" href="#">
                                        <i class="fa fa-share-alt" aria-hidden="true"></i> Share
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="submit" class="btn btn-success" value="Save"/>
    <?php ActiveForm::end();?>

</div>
</div>
<style>
    #userphotos-cover_image {
        display: none;
    }

    #userphotos-profile_image{
        display: none;
    }


    #userphotos-other_image{
        display: none;
    }

</style>
<style>
    .field-userphotos-cover_image{
        padding-left:10px;
        padding-top:5px;
        cursor: pointer;
    }

    #test:hover .field-userphotos-cover_image {
        background-color: black;
        transition: 0.3s;
        border-radius: 2px;
        border:1px solid white;
    }

    #test:hover .field-userphotos-cover_image {
        #hidden-cover-image-label-text {
            display: block;
        }
    }
</style>
<script>
    $(document).ready(function(){
        $("#profile_image_box").on('click',function(){
            $(".block-profile-image").show();
        });

        $("#close-profile-image-block").on("click",function(){
            $(".block-profile-image").hide();
        });
    });
</script>


