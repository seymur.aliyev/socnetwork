<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'My Yii Application';
?>
<div class="col-md-9">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>

   <!-- <?php /*$_SESSION["csrf_token"] = md5(rand(0,10000000)).time();*/?>
    <input type="hidden" name="csrf_token"
           value="<?/*=htmlspecialchars($_SESSION["csrf_token"]);*/?>"/>-->

    <div class="row cover-image" style="height: 315px;border: 1px solid #d2d1d1;">
            <a href="<?=Url::to(['common/photo','id'=> $current_cover_image['id'],
                'user_id'=> $user_info['id'] ]);?>">
                <img style='width: 100%;max-height: 100%;' src="<?=(Yii::getAlias('@web').'/web/uploads/cover_image/'.($current_cover_image['hash_name'] ? $current_cover_image['hash_name'] : 'no-image.png'));?>" alt=""/>
            </a>
            <div class="col-md-12" style="position: absolute;top: 130px;">
            <div class="col-md-3 profile-image">
                <div style="height: 168px;width:168px;">

                    <a href="<?=Url::to(['common/photo','id'=> $current_profile_image['id'],
                        'user_id'=> $user_info['id'] ]);?>">
                        <img src="<?=(Yii::getAlias('@web').'/web/uploads/profile_image/'.($current_profile_image['hash_name'] ? $current_profile_image['hash_name'] : 'no-image.png'));?>" alt="" style="
                            width: 168px;height: 168px;border-radius: 5px 20px 5px;border: 2px solid white;"/>
                    </a>
                </div>
            </div>
            <div class="col-md-3 profile-name" style="padding-left: 0;">
                <h1 style="color: white;">
                    <a style="text-decoration: none;color:white;"
                       href="<?= Url::toRoute(['others/profile','id'=> $user_info['id']]);?>"><?=$user_info['name'];?>
                    </a>
                </h1>
            </div>
            <div class="col-md-3" style="float: right;padding-top: 125px;text-align: right;">
                <input type="submit" name="send_friend_request"
                        class="btn btn-default" value="<?=$friendship_status?>"/>
            </div>
        </div>
    </div>


    <div class="row" style="padding-left: 0;padding-right: 0;margin-top: 15px;">
        <div class="col-md-5" style="padding-left: 0;">
            <div class="intro" >
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Intro
                    </div>
                    <div class="panel-body">
                        <?php foreach ($allWorkPlaces as $workPlace) {?>
                            <div class="col-md-12">
                                <i class="fa fa-briefcase"></i> <?=$workPlace['position'];?> at <?=$workPlace['company'];?>
                            </div>
                        <?php } ?>
                        <?php foreach ($allUniversities as $university) {?>
                            <div class="col-md-12">
                                <i class="fa fa-graduation-cap"></i> studied at <?=$university['name'];?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="friends">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-12">
                                <a style="" href="<?=Url::toRoute(['others/friends',
                                    'id'=> Yii::$app->request->get('id')]);?>">
                                    <i class="fa fa-users"></i> Friends
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php foreach ($friends as $friend) {?>
                            <div class="col-md-4">
                                <img src="<?=(Yii::getAlias('@web').'/web/uploads/profile_image/'.($friend['hash_name'] ? $friend['hash_name'] : 'no-image.png'));?>" alt=""
                                     style="border: 2px solid white;width: 100%;height: 100%;">
                                <?php if($friend['id'] == Yii::$app->user->identity['id']){?>
                                <a style="position: absolute;bottom: 0;left: 20px;color:white;" href="<?=Url::toRoute(['/'.Yii::$app->user->identity['name']]); ?>">
                                    <?=$friend['name'];?>
                                </a>
                                <?php } else { ?>
                                    <a style="position: absolute;bottom: 0;left: 20px;color:white;"
                                       href="<?=Url::toRoute(['others/profile','id' => $friend['id'] ]); ?>">
                                        <?=$friend['name'];?>
                                    </a>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="photos">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="<?= Url::toRoute(['others/photo-albums',
                                    'id'=> Yii::$app->request->get('id')]);?>"
                                    <i class="fa fa-file-image-o"></i> Photos
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php if(!empty($user_photos)) {
                            foreach ($user_photos as $k => $photo) { ?>
                                <div class="col-md-3">
                                    <img style="max-height: 100%;width: 100%;" src="<?=Yii::getAlias('@web').'/web/uploads/profile_image/'.$photo['hash_name'];?>" alt="">
                                </div>
                        <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-7" style="padding-right: 0;">
            <div class="user-timeLine">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row" style="height: 20px;">
                            <div class="col-md-3" style="height: 100%;padding-left:25px;border-right: 1px solid black;">
                                <?= $form->field($userPhotoModel, 'other_image', [
                                ])->fileInput()
                                    ->label('
                                    <div class="col-md-12"  style="cursor: pointer;padding-left:0;">
                                    <div class="col-md-6" style="padding-left: 0;padding-right: 0;text-align: center;">
                                        <i id="content-image-icon" style="font-size: 20px;opacity:0.9;cursor: pointer;" 
                                        class="fa fa-camera-retro"></i>
                                        </div>
                                        <div class="col-md-6" style="padding-left: 0;">
                                        <p>Photo</p>
                                        </div>
                                    </div>
                                 ');
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                            <div class="col-md-2">

                                <a href="<?=Url::to(['common/photo','id'=> $current_profile_image['id'],
                                    'user_id'=> $user_info['id'] ]);?>">
                                <img class="image-circle" src="<?=(Yii::getAlias('@web').'/web/uploads/profile_image/'.($current_profile_image['hash_name'] ? $current_profile_image['hash_name'] : 'no-image.png'));?>" alt=""
                                     style="border-radius: 50%;width:40px;height:40px;"/>
                                </a>
                            </div>
                            <div class="col-md-10" style="padding-left: 0;">
                                <textarea style="resize:none;width: 100%;height: 90px;border: none;outline:none;"
                                          placeholder="What is in your mind?" name="" id=""></textarea>
                            </div>
                    </div>

                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-8" style="float: right;padding-right: 0;">

                                <div class="col-md-4" style="float: right;">
                                    <button class="btn btn-primary" style="float: right;">POST</button>
                                </div>

                                <div class="col-md-6" style="float: right;">
                                    <select class="form-control" name="" id="">
                                        <option value="">Only me</option>
                                        <option value="">Public</option>
                                        <option value="">Friends</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="user-content">
                <div class="post">
                    <div class="panel panel-default">
                        <div class="panel-heading">Content</div>
                        <div class="panel-body">
                            Content body
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-md-4" style="text-align: center;">
                                    <a href="#">
                                        <i class="fa fa-thumbs-o-up"></i> Like
                                    </a>

                                </div>
                                <div class="col-md-4" style="text-align: center;">
                                    <a href="#">
                                        <i class="fa fa-comment-o"></i> Comment
                                    </a>
                                </div>
                                <div class="col-md-4" style="text-align: center;">
                                    <a style="" href="#">
                                        <i class="fa fa-share-alt" aria-hidden="true"></i> Share
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="submit" class="btn btn-success" value="Save"/>
    <?php ActiveForm::end();?>

</div>
<style>
    #userphotos-cover_image {
        display: none;
    }

    #userphotos-profile_image{
        display: none;
    }


    #userphotos-other_image{
        display: none;
    }

</style>
<style>
    .field-userphotos-cover_image{
        padding-left:10px;
        padding-top:5px;
        cursor: pointer;
    }

    #test:hover .field-userphotos-cover_image {
        background-color: black;
        transition: 0.3s;
        border-radius: 2px;
        border:1px solid white;
    }

    #test:hover .field-userphotos-cover_image {
        #hidden-cover-image-label-text {
            display: block;
        }
    }
</style>
<script>
    $(document).ready(function(){
        $("#profile_image_box").on('click',function(){
            $(".block-profile-image").show();
        });

        $("#close-profile-image-block").on("click",function(){
            $(".block-profile-image").hide();
        });
    });
</script>


