<?php

/* @var $this yii\web\View */

use app\models\Friend;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'My Yii Application';
?>
<div class="col-md-9">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>

    <?php $_SESSION["csrf_token"] = md5(rand(0,10000000)).time();?>
    <input type="hidden" name="csrf_token"
           value="<?=htmlspecialchars($_SESSION["csrf_token"]);?>"/>

    <!-- Cover and profile images block starts -->
    <div id="test" class="row cover-image" style="height: 315px;border: 1px solid #d2d1d1;">
        <img style='width: 100%;max-height: 100%;' src="<?=(Yii::getAlias('@web').'/web/uploads/cover_image/'.($current_cover_image ? $current_cover_image : 'no-image.png'));?>" alt=""/>
        <div class="col-md-12" style="position: absolute;top: 130px;">
            <div class="col-md-3 profile-image">
                <div style="height: 168px;width:168px;">
                    <img src="<?=(Yii::getAlias('@web').'/web/uploads/profile_image/'.($current_profile_image ? $current_profile_image : 'no-image.png'));?>" alt="" style="
                            max-height: 100%;width:100%;
                            border-radius: 5px 20px 5px;border: 2px solid white;
                                ">
                </div>
            </div>
            <div class="col-md-3 profile-name" style="padding-left: 0;">
                <h1 style="color: white;">
                    <a style="text-decoration: none;color:white;"
                       href="<?= Url::toRoute(['others/profile','id'=> $user_info['id']]);?>"><?=$user_info['name'];?>
                    </a>
                </h1>
            </div>

            <div class="col-md-3" style="float: right;padding-top: 125px;text-align: right;">
                <input type="submit" name="send_friend_request"
                       class="btn btn-default" value="<?=$friendship_status?>"/>
            </div>
        </div>
    </div>
    <!-- Cover and profile images block ends -->

    <div class="row" style="margin-top: 30px;">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-users"></i> Friends
            </div>
            <div class="panel-body">
                <?php foreach ($friends as $friend) {?>
                    <div class="col-md-6" style="border:1px solid gray;border-radius:2px;padding-left: 0;">
                        <div class="col-md-4" style="padding-left: 0;">
                            <img src="<?=(Yii::getAlias('@web').'/web/uploads/profile_image/'.($friend['hash_name'] ? $friend['hash_name'] : 'no-image.png'));?>" alt=""
                             style="border: 2px solid white;width: 100px;height: 100px;">
                        </div>
                        <div class="col-md-4" style="margin-top: 45px;font-size: 16px;">
                            <?php if($friend['id'] == Yii::$app->user->identity['id']){?>
                                <a href="<?=Url::toRoute(['/'.Yii::$app->user->identity['name']]); ?>">
                                    <b><?=$friend['name'];?></b>
                                </a>
                            <?php } else { ?>
                                <a href="<?=Url::toRoute(['others/profile','id' => $friend['id'] ]); ?>">
                                    <b><?=$friend['name'];?></b>
                                </a>
                            <?php } ?>
                        </div>
                        <?php if($friend['id']!=Yii::$app->user->identity['id']){?>
                        <div class="col-md-4" style="padding-top: 40px;">
                            <?php
                                $friendModel = new Friend();
                                $friendshipAmongFriendsOfFriend = $friendModel->getFriendshipStatusForUser(Yii::$app->user->identity['id'],$friend['id']);
                                if($friendshipAmongFriendsOfFriend == 'Add friend'){ ?>
                                    <a data-friend-id="<?=$friend['id'];?>" class="btn btn-primary add-friend">
                                        <i class="fa fa-plus"></i> Add friend
                                    </a>
                                <?php } else if ($friendshipAmongFriendsOfFriend == 'Friend request sent') { ?>
                                    <a class="btn btn-default">
                                        <i class="fa fa-user"></i> Request sent
                                    </a>
                                <?php } else if ($friendshipAmongFriendsOfFriend == 'Friends'){ ?>

                                    <a data-friend-id="<?=$friend['id'];?>" class="btn btn-primary add-friend" style="display: none;">
                                        <i class="fa fa-plus"></i> Add friend
                                    </a>
                                    <div class="dropdown">
                                        <a class="btn btn-default dropbtn">
                                            <i class="fa fa-check"></i> Friends
                                        </a>
                                        <div class="dropdown-content">
                                            <a style="cursor: pointer;text-align:center;" type="button" class="unfriend" data-friend-id="<?=$friend['id'];?>">Unfriend</a>
                                        </div>
                                    </div>
                                <?php } ?>
                        </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end();?>

</div>
<style>
    #userphotos-cover_image {
        display: none;
    }

    #userphotos-profile_image{
        display: none;
    }


    #userphotos-other_image{
        display: none;
    }

</style>
<style>
    .field-userphotos-cover_image{
        padding-left:10px;
        padding-top:5px;
        cursor: pointer;
    }

    #test:hover .field-userphotos-cover_image {
        background-color: black;
        transition: 0.3s;
        border-radius: 2px;
        border:1px solid white;
    }

    #test:hover .field-userphotos-cover_image {
        #hidden-cover-image-label-text {
            display: block;
        }
    }
</style>

<style>
    .dropbtn {
        background-color: #4CAF50;
        color: white;
        /*padding: 16px;*/
        /*font-size: 16px;*/
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }

    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    .dropdown-content a:hover {
        background-color: #ddd
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }

    .dropdown:hover .dropbtn {
        background-color: #3e8e41;
    }
</style>

<script>
    $(document).ready(function(){
        $("#profile_image_box").on('click',function(){
            $(".block-profile-image").show();
        });

        $("#close-profile-image-block").on("click",function(){
            $(".block-profile-image").hide();
        });

        $(".add-friend").on('click',function(){
            var friend_id = $(this).data('friend-id');
            var url = "<?=Yii::getAlias('@web');?>/ajax/add-friend/?friend_id="+encodeURIComponent(friend_id)+
                '&user_id='+ <?=Yii::$app->user->identity['id'];?>;
            $.ajax({
                url: url,
                type: "GET",
                async: true,
                cache: false,
                dataType: 'json',
                success: function(response){
                    if(response.success){
                        $(".dropdown").hide();
                        $(".add-friend").text("Request sent");
                    }
                },
                error: function(){
                    console.log("error");
                }
            });
        });
    });

    $(".unfriend").on('click',function(){
        var friendId = $(this).data('friend-id');
        var url = "<?=Yii::getAlias('@web');?>/ajax/unfriend/?id="+friendId+
            '&user_id='+<?=Yii::$app->user->identity['id'];?>;
        if (friendId){
            $.ajax({
                url: url,
                type: "GET",
                async: true,
                cache: false,
                dataType: 'json',
                success: function (response){
                    if (response.success){
                        $(".dropbtn").hide();
                        $(".add-friend").show();
                    }
                },
                error: function () {
                    console.log("error");
                }
            });
        }
    });
</script>


