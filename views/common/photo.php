<?php
use yii\helpers\Url;
?>
<div class="row">
    <div class="target-image-box col-md-9">
        <img class="img-responsive" src="<?= Yii::getAlias('@web').'/web/uploads/'.$photo['type'].'/'.$photo['hash_name'];?>" alt=""/>
    </div>

    <div class="col-md-3" style="background:green;">
        <?php
        $urlToController = '';
        if($userInfo['id'] == Yii::$app->user->identity->id){
            $urlToController = 'profile/index';
        }
        else {
            $urlToController = 'others/profile';
        }

        ?>
        <a href="<?=Url::to([$urlToController, 'id' => $userInfo['id'] ]);?>">
            <img class="image-circle" src="<?=(Yii::getAlias('@web').'/web/uploads/profile_image/'.($current_profile_image['hash_name'] ? $current_profile_image['hash_name'] : 'no-image.png'));?>" alt="">
            <h4><?=$userInfo['name'];?></h4>
        </a>
    </div>
</div>
