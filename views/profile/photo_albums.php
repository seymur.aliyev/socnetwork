<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'My Yii Application';
?>
<div class="col-md-9">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>
    <?php $_SESSION["csrf_token"] = md5(rand(0,10000000)).time();?>
    <input type="hidden" name="csrf_token" value="<?=
    htmlspecialchars($_SESSION["csrf_token"]);?>"
    />

    <div class=" row cover-image" style="height: 315px;border: 1px solid #d2d1d1;">
        <img style='width: 100%;max-height: 100%;' src="<?=(Yii::getAlias('@web').'/web/uploads/cover_image/'.($current_cover_image ? $current_cover_image : 'no-image.png'));?>" alt=""/>

        <div class="col-md-12" style="padding: 10px;
    position: absolute;
    top: 0;">
            <div class="col-md-4" style="padding: 10px;position: absolute;top: 0px;padding-left: 25px;">

                <?= $form->field($userPhotoModel, 'cover_image', [
                ])->fileInput()
                    ->label('<i id="cover-image-icon" style="font-size: 30px;opacity:0.9;cursor: pointer;" class="fa fa-camera-retro"></i>')
                ?>

            </div>
        </div>

        <div class="col-md-12" style="position: absolute;top: 130px;">

            <div class="col-md-3 profile-image">
                <div style="height: 168px;width:168px;">
                    <img src="<?=(Yii::getAlias('@web').'/web/uploads/profile_image/'.($current_profile_image ? $current_profile_image : 'no-image.png'));?>" alt="" style="
                            max-height: 100%;width:100%;
                            border-radius: 5px 20px 5px;border: 2px solid white;
                                ">
                </div>
                <div class="col-md-12" style="bottom: 46px;height: 45px;background: #504a4a;">
                    <?= $form->field($userPhotoModel, 'profile_image', [
                    ])->fileInput()
                        ->label('<p style="color:white;cursor: pointer;"><i id="profile-image-icon" style="font-size: 20px;opacity:0.9;color:white;" class="fa fa-camera-retro"></i> Upload profile</p>');
                    ?>
                </div>
            </div>

            <div class="col-md-3 profile-name" style="padding-left: 0;">
                <h1 style="color: white;">
                    <a style="text-decoration: none;color:white;"
                       href="<?= Url::toRoute(['profile/index']);?>"><?=Yii::$app->user->identity['name'];?>
                    </a>
                </h1>
            </div>
        </div>
    </div>


    <div class="row" style="padding-right: 0;padding-left: 0;margin-top: 30px;">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-file-image-o"></i> Photos
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">

                    <?php
                    $byType = [];
                    foreach ($all_photos as $k => $data) {
                        $byType[$data['type']][] = $data;
                    }
                    ?>


                    <?php
                    foreach ($byType as $k => $images) {

                        foreach ($images as $image) {?>
                            <div class="col-md-3" style="padding-left: 0;padding-top: 10px;">
                                <a href="<?=Url::to(['common/photo','id'=> $image['id']]);?>">
                                    <img style="width:100%;height: 206px;"
                                     src="<?=(Yii::getAlias('@web').'/web/uploads/'.$k.'/'.($image['hash_name'] ? $image['hash_name'] : 'no-image.png'));?>" alt=""/>
                                </a>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end();?>

</div>
<style>
    #userphotos-cover_image {
        display: none;
    }

    #userphotos-profile_image{
        display: none;
    }


    #userphotos-other_image{
        display: none;
    }
</style>
