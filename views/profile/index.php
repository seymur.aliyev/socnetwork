<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
$this->title = 'Profile';
?>

<div class="row">
    <div class="col-md-9">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>
            <?php /*$_SESSION["csrf_token"] = md5(rand(0,10000000)).time();*/?><!--
            <input type="hidden" name="csrf_token"
               value="<?/*=htmlspecialchars($_SESSION["csrf_token"]);*/?>"/>-->
            <!-- cover and profile images block starts -->
            <div class="cover-image-block">
                <a href="<?=Url::to(['common/photo','id'=> $current_cover_image['id'],'user_id'=> Yii::$app->user->identity->id]);?>">
                    <img class="cover-image-block__image"
                          src="<?=(Yii::getAlias('@web').'/web/uploads/cover_image/'.($current_cover_image['hash_name'] ? $current_cover_image['hash_name'] : 'no-image.png'));?>" alt=""/>
                </a>
                <!--<div id="myModal" class="modal">
                    <span class="close">&times;</span>
                    <img class="modal-content" id="img01">
                    <div id="caption"></div>
                </div>-->
                <!--<script>
                    // Get the modal
                    var modal = document.getElementById('myModal');

                    // Get the image and insert it inside the modal - use its "alt" text as a caption
                    var img = document.getElementById('myImg');
                    var modalImg = document.getElementById("img01");
                    var captionText = document.getElementById("caption");
                    img.onclick = function(){
                        modal.style.display = "block";
                        modalImg.src = this.src;
                        captionText.innerHTML = this.alt;
                    }

                    // Get the <span> element that closes the modal
                    var span = document.getElementsByClassName("close")[0];

                    // When the user clicks on <span> (x), close the modal
                    span.onclick = function() {
                        modal.style.display = "none";
                    }
                </script>-->

                <!--<style>
                    #myImg {
                        border-radius: 5px;
                        cursor: pointer;
                        transition: 0.3s;
                    }

                    #myImg:hover {opacity: 0.7;}

                    /* The Modal (background) */
                    .modal {
                        display: none; /* Hidden by default */
                        position: fixed; /* Stay in place */
                        z-index: 1; /* Sit on top */
                        padding-top: 100px; /* Location of the box */
                        left: 0;
                        top: 0;
                        width: 100%; /* Full width */
                        height: 100%; /* Full height */
                        overflow: auto; /* Enable scroll if needed */
                        background-color: rgb(0,0,0); /* Fallback color */
                        background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
                    }

                    /* Modal Content (image) */
                    .modal-content {
                        margin: auto;
                        display: block;
                        width: 80%;
                        max-width: 700px;
                    }

                    /* Caption of Modal Image */
                    #caption {
                        margin: auto;
                        display: block;
                        width: 80%;
                        max-width: 700px;
                        text-align: center;
                        color: #ccc;
                        padding: 10px 0;
                        height: 150px;
                    }

                    /* Add Animation */
                    .modal-content, #caption {
                        -webkit-animation-name: zoom;
                        -webkit-animation-duration: 0.6s;
                        animation-name: zoom;
                        animation-duration: 0.6s;
                    }

                    @-webkit-keyframes zoom {
                        from {-webkit-transform:scale(0)}
                        to {-webkit-transform:scale(1)}
                    }

                    @keyframes zoom {
                        from {transform:scale(0)}
                        to {transform:scale(1)}
                    }

                    /* The Close Button */
                    .close {
                        position: absolute;
                        top: 15px;
                        right: 35px;
                        color: #f1f1f1;
                        font-size: 40px;
                        font-weight: bold;
                        transition: 0.3s;
                    }

                    .close:hover,
                    .close:focus {
                        color: #bbb;
                        text-decoration: none;
                        cursor: pointer;
                    }

                    /* 100% Image Width on Smaller Screens */
                    @media only screen and (max-width: 700px){
                        .modal-content {
                            width: 100%;
                        }
                    }
                </style>-->


                <div class="col-md-12" style="padding: 10px;position: absolute;top: 0;">
                    <!-- Block to config cover image -->
                    <div class="col-md-4 config-cover-image">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i class="fa fa-camera-retro"></i> Update cover image
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li>
                                    <a class="config-cover-image__dropdown-items__select-item">
                                        Select image
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <?= $form->field($userPhotoModel, 'cover_image', [
                                        ])->fileInput([
                                                'onchange' => 'uploadCover(this)']
                                        )->label('Upload') ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?=Url::toRoute(['profile/remove-cover-image','hash_name' => $current_cover_image]);?>" class="col-md-12 config-cover-image__remove">
                                        <label for="">Remove</label>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Block to config cover image -->

                    <!-- Block to select cover image from existing cover images -->
                    <div class="cover-image-block__select panel panel-default" style="display:none;">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <h5><b>Select Cover Image</b></h5>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="#" class="cover-image-block__select__close correct_close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12 cover-image-block__select__images" style="padding-left: 0;padding-right: 0;">
                                <?php
                                $byType = [];
                                foreach ($all_photos as $k => $data) {
                                    $byType[$data['type']][] = $data;
                                }
                                ?>
                                <?php foreach ($byType as $k => $images) {?>
                                    <div class="col-md-12">
                                        <?php
                                        if($k == 'cover_image') {
                                            foreach ($images as $image) { ?>
                                                <div class="col-md-3">
                                                    <input class="profile-image-block__input_hidden" type='radio'
                                                           value="<?= $image['id']; ?>" name='chooseCoverImage'
                                                           id="coverImageRadio<?= $image['id']; ?>"/>
                                                    <label for="coverImageRadio<?= $image['id']; ?>">
                                                        <a type="button" class="btn btn-default">
                                                            <img style="height: 112px;width: 112px;"
                                                                 src="<?= Yii::getAlias('@web') . '/web/uploads/' . $k . '/' . $image['hash_name']; ?>"
                                                                 alt="">
                                                        </a>
                                                    </label>
                                                </div>
                                            <?php }
                                        }
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="submit" value="Use this image" class="btn btn-primary"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Block to select cover image from existing cover images -->
                </div>

                <!-- Profile image block starts -->
                <div class="col-md-12" style="position: absolute;top: 130px;">
                    <div class="col-md-3 profile-image">
                        <div style="height: 168px;width:168px;">
                            <a href="<?=Url::to(['common/photo','id' => $current_profile_image['id'], 'user_id' => Yii::$app->user->identity->id]);?>">
                                <img src="<?=(Yii::getAlias('@web').'/web/uploads/profile_image/'.($current_profile_image['hash_name'] ? $current_profile_image['hash_name'] : 'no-image.png'));?>" alt="" style="
                                            max-height: 100%;width:100%;
                                            border-radius: 5px 20px 5px;border: 2px solid white;
                                                ">
                            </a>
                                <div class="col-md-12" style="bottom: 46px;height: 45px;background: #504a4a;">
                                <a class='btn btn-default' id="profile_image_box" >Upload Image</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 profile-name" style="padding-left: 0;">
                        <h1 style="color: white;">
                            <a style="text-decoration: none;color:white;"
                               href="<?= Url::toRoute(['/'.Yii::$app->user->identity['name']]);?>"><?=Yii::$app->user->identity['name'];?>
                            </a>
                        </h1>
                    </div>
                    <div class="col-12 cover-image-upload-submit" style="margin-top: 140px;text-align: right;display:none;">
                        <input type="button" class="btn btn-default" value="Cancel" onClick="window.location.reload()">
                        <input  type="submit" class="btn btn-success" value="Save"/>
                    </div>

                    <!-- Block profile image block -->
                    <div class="profile-image-block panel panel-default" style="display:none;">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <h5><b>Update Profile Image</b></h5>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="#" class="profile-image-block__close correct_close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="col-md-6 profile-image-block__upload">
                                <?= $form->field($userPhotoModel, 'profile_image', [
                                ])->fileInput(['onchange' => 'imagepreview(this)'])->label();
                                ?>
                            </div>
                            <div class="col-md-12 profile-image-block__images" style="padding-left: 0;padding-right: 0;">
                                <?php
                                $byType = [];
                                foreach ($all_photos as $k => $data) {
                                    $byType[$data['type']][] = $data;
                                }
                                ?>
                                <?php foreach ($byType as $k => $images) {?>
                                    <div class="col-md-12" style="padding-left: 0;">
                                        <div class="col-md-12">
                                            <h4><?=Yii::t('app','category_'.$k);?></h4>
                                        </div>
                                        <br/>
                                        <?php foreach ($images as $image) {?>
                                            <div class="col-md-3">
                                                <input class="profile-image-block__input_hidden" type='radio' value="<?=$image['id'];?>" name='chooseProfileImage' id="radio<?=$image['id'];?>"/>
                                                <label for="radio<?=$image['id'];?>">
                                                    <a type="button" class="btn btn-default">
                                                        <img style="height: 112px;width: 112px;" src="<?=Yii::getAlias('@web').'/web/uploads/'.$k.'/'.$image['hash_name'];?>" alt="">
                                                    </a>
                                                </label>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="submit" value="Use this image" class="btn btn-primary"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Block profile image block -->


                    <!-- Block to upload new profile image -->
                    <div class="block-create-profile-image" style="display: none;">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <b>Create profile picture</b>
                            </div>
                            <div class="panel-body">
                                <div class="col-12" style="text-align: center;">
                                    <img src="" id="output" style="width: 200px;height: 180px;" alt="">
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6" style="padding-right: 0;float: right;text-align:right;">
                                            <input type="button" class="btn btn-default" value="Cancel" onClick="window.location.reload()">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Block to upload new profile image -->
                </div>
                <!-- Profile image block ends -->
            </div>
            <!-- cover and profile images block ends -->
        <?php $form->end();?>

        <div class="col-12" style="padding-left: 0;padding-right: 0;margin-top: 15px;">
            <div class="col-md-5" style="padding-left: 0;">
                <div class="intro" >
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-12">
                                    <a style="" href="<?=Url::toRoute([Yii::$app->user->identity['name'].'/about']);?>">
                                        <i class="fa fa-user"></i> About
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <?php foreach ($allWorkPlaces as $workPlace) {?>
                                <div class="col-md-12">
                                    <i class="fa fa-briefcase"></i> <?=$workPlace['position'];?> at <?=$workPlace['company'];?>
                                </div>
                            <?php } ?>
                            <?php foreach ($allUniversities as $university) {?>
                                <div class="col-md-12">
                                    <i class="fa fa-graduation-cap"></i> studied at <?=$university['name'];?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="friends">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-12">
                                    <a style="" href="<?=Url::toRoute([Yii::$app->user->identity['name'].'/friends']);?>">
                                        <i class="fa fa-users"></i> Friends
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <?php foreach ($friends as $friend) {?>
                                <div class="col-md-4">
                                    <img src="<?=(Yii::getAlias('@web').'/web/uploads/profile_image/'.($friend['hash_name'] ? $friend['hash_name'] : 'no-image.png'));?>" alt=""
                                         style="border: 2px solid white;width: 100%;height: 100%;">
                                    <a style="position: absolute;bottom: 0;left: 20px;color:white;"
                                       href="<?=Url::toRoute(['others/profile','id' => $friend['id'] ]); ?>">
                                        <?=$friend['name'];?>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="photos">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-12">
                                    <a style="" href="<?=Url::toRoute([Yii::$app->user->identity['name'].'/photo-albums']);?>">
                                        <i class="fa fa-users"></i> Photos
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <?php if(!empty($user_photos)) {
                                foreach ($user_photos as $k => $photo) { ?>
                                    <div class="col-md-3">
                                        <img style="max-height: 100%;width: 100%;" src="<?=Yii::getAlias('@web').'/web/uploads/profile_image/'.$photo['hash_name'];?>" alt="">
                                    </div>
                            <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-7" style="padding-right: 0;">
                <div class="user-timeLine">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row" style="height: 20px;">
                                <div class="col-md-3" style="height: 100%;padding-left:25px;border-right: 1px solid black;">
                                    <?= $form->field($userPhotoModel, 'other_image', [
                                    ])->fileInput()
                                        ->label('
                                        <div class="col-md-12"  style="cursor: pointer;padding-left:0;">
                                        <div class="col-md-6" style="padding-left: 0;padding-right: 0;text-align: center;">
                                            <i id="content-image-icon" style="font-size: 20px;opacity:0.9;cursor: pointer;" 
                                            class="fa fa-camera-retro"></i>
                                            </div>
                                            <div class="col-md-6" style="padding-left: 0;">
                                            <p>Photo</p>
                                            </div>
                                        </div>
                                     ');
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                                <div class="col-md-2">
                                    <a href="<?=Url::to(['common/photo','id' => $current_profile_image['id'], 'user_id' => Yii::$app->user->identity->id]);?>">
                                        <img class="image-circle" src="<?=(Yii::getAlias('@web').'/web/uploads/profile_image/'.($current_profile_image['hash_name'] ? $current_profile_image['hash_name'] : 'no-image.png'));?>" alt=""/>
                                    </a>
                                </div>
                                <div class="col-md-10" style="padding-left: 0;">
                                    <textarea style="resize:none;width: 100%;height: 90px;border: none;outline:none;"
                                              placeholder="What is in your mind?" name="" id=""></textarea>
                                </div>
                        </div>

                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-md-8" style="float: right;padding-right: 0;">

                                    <div class="col-md-4" style="float: right;">
                                        <button class="btn btn-primary" style="float: right;">POST</button>
                                    </div>

                                    <div class="col-md-6" style="float: right;">
                                        <select class="form-control" name="" id="">
                                            <option value="">Only me</option>
                                            <option value="">Public</option>
                                            <option value="">Friends</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="user-content">
                    <div class="post">
                        <div class="panel panel-default">
                            <div class="panel-heading">Content</div>
                            <div class="panel-body">
                                Content body
                            </div>
                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-md-4" style="text-align: center;">
                                        <a href="#">
                                            <i class="fa fa-thumbs-o-up"></i> Like
                                        </a>

                                    </div>
                                    <div class="col-md-4" style="text-align: center;">
                                        <a href="#">
                                            <i class="fa fa-comment-o"></i> Comment
                                        </a>
                                    </div>
                                    <div class="col-md-4" style="text-align: center;">
                                        <a style="" href="#">
                                            <i class="fa fa-share-alt" aria-hidden="true"></i> Share
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<style>
    #userphotos-cover_image {
        display: none;
    }

    #userphotos-profile_image{
        /*display: none;*/
    }


    #userphotos-other_image{
        display: none;
    }

</style>
<style>
    .field-userphotos-cover_image{
        padding-left:10px;
        padding-top:5px;
        cursor: pointer;
    }
</style>

<script>
    $(document).ready(function(){
        $("#profile_image_box").on('click',function(){
            $(".profile-image-block").show();
        });

        $('.config-cover-image__dropdown-items__select-item').on('click',function(){
            $(".cover-image-block__select").show();
        });

        $(".profile-image-block__close").on("click",function(){
            $(".profile-image-block").hide();
        });

        $(".cover-image-block__select__close").on('click',function(){
            $(".cover-image-block__select").hide();
        });


        $(".config-cover-image__button").on('click',function(){
            $(".config-cover-image__dropdown-items").toggle();
            return false;
        });

        $(".config-cover-image__remove").on('click',function(){
            return confirm('Are you sure to remove ?');
        })
    });

    function imagepreview(input) {
        var file = input.files[0];
        var fileType = file["type"];
        var size = file["size"];
        var ext = ["image/gif", "image/jpeg", "image/png"];

        if (input.files && file) {
            var reader = new FileReader();

            if ($.inArray(fileType, ext) < 0) {
                alert('Допускаемые форматы :JPG/GIF/PNG');

            } else if (size > 2097152) {
                alert('Не больше 2 мб');

            } else {
                $(".block-create-profile-image").show();
                reader.onload = function (e) {
                    $("#output").attr('src', e.target.result);
                };
                reader.readAsDataURL(file);
            }
        }
    }

    function uploadCover(input) {
        var file = input.files[0];
        var fileType = file["type"];
        var size = file["size"];
        var ext = ["image/gif", "image/jpeg", "image/png"];
        if (input.files && file) {
            var reader = new FileReader();
            if ($.inArray(fileType, ext) < 0) {
                alert('Допускаемые форматы :JPG/GIF/PNG');
            } else if (size > 2097152) {
                alert('Не больше 2 мб');
            } else {
                $(".cover-image-upload-submit").show();
                reader.onload = function (e) {
                    $(".cover-image-block__image").attr('src', e.target.result);
                };
                reader.readAsDataURL(file);
            }
        }
    }
</script>
