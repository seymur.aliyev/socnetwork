<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 15.06.2018
 * Time: 7:30
 */

namespace app\controllers;
use app\models\User;
use app\models\UserPhotos;
use Yii;
use yii\base\Controller;

class CommonController extends Controller
{
    public function actionPhoto(){
        $id = Yii::$app->request->get('id');
        $user_id = Yii::$app->request->get('user_id');

        $userPhotoModel = new  UserPhotos();
        $userModel = new User();

        $photo = $userPhotoModel->showPhoto($id);
        $userInfo = $userModel->getUserInfo($user_id);

        $current_profile_image = $userPhotoModel->getProfileImage($user_id);

        return $this->render('photo',[
            'photo' => $photo,
            'userInfo' => $userInfo,
            'current_profile_image' => $current_profile_image
        ]);
    }

}