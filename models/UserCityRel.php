<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_city_rel".
 *
 * @property int $id
 * @property int $user_id
 * @property int $city_id
 * @property string $is_current
 * @property string $is_home_town
 */
class UserCityRel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_city_rel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'],'required'],
            [['user_id', 'city_id'], 'integer'],
            [['is_current', 'is_home_town'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'city_id' => Yii::t('app', 'City'),
            'is_current' => Yii::t('app', 'Is Current'),
            'is_home_town' => Yii::t('app', 'Is Home Town'),
        ];
    }

    public function addUserCity($userCityData){
        $city = new UserCityRel();
        $city->user_id = $userCityData['user_id'];
        $city->city_id = $userCityData['city_id'];
        if($userCityData['is_current']){
            $city->is_current = $userCityData['is_current'];
        }
        if($userCityData['is_home_town']){
            $city->is_home_town = $userCityData['is_home_town'];
        }
        $city->save(false);
    }

    public function getCurrentCity($user_id){
        $where = $params = [];
        $where[] = "ucr.user_id = :user_id ";
        $where[] = "ucr.is_current = '1' ";
        $params['user_id'] = $user_id;

        $sql = 'SELECT  ucr.*, cl.name as name
                FROM user_city_rel ucr
                LEFT JOIN  city_list cl ON cl.id=ucr.city_id
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryOne();
    }


    public function getHomeTown($user_id){
        $where = $params = [];
        $where[] = "ucr.user_id = :user_id ";
        $where[] = "ucr.is_home_town = '1' ";
        $params['user_id'] = $user_id;

        $sql = 'SELECT ucr.*, cl.name as name
                FROM user_city_rel ucr
                LEFT JOIN  city_list cl ON cl.id=ucr.city_id
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryOne();
    }

    public function removeCity($id){
        $where = $params = [];
        $where[] = "id = :id";
        $params['id'] = $id;

        $sql = 'DELETE FROM user_city_rel
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->execute();
    }
}
