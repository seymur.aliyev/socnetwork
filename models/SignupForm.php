<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 08.04.2018
 * Time: 23:11
 */

namespace app\models;
use yii\base\Model;
use app\models\User;

class SignupForm extends Model
{
    public $name;
    public $email;
    public $password;

    public function rules(){

       return [
           [['name','email','password'],'required'],
           [['email'],'email'],
           [['name'],'string'],
           [['email'],'unique','targetClass' => 'app\models\User','targetAttribute' => 'email']
       ];
    }

    public function signup(){

        if($this->validate()){
            $user = new User();
            $user->attributes = $this->attributes;
            //var_dump($user);die;
            return $user->create();
        }
    }

}