<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "university_list".
 *
 * @property int $id
 * @property string $name
 * @property int $code
 * @property string $type
 * @property string $is_ministry_slave
 */
class UniversityList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'university_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'type'], 'required'],
            [['code'], 'integer'],
            [['type', 'is_ministry_slave'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
            'type' => Yii::t('app', 'Type'),
            'is_ministry_slave' => Yii::t('app', 'Is Ministry Slave'),
        ];
    }


    public function getUniversityList(){
        $query = UniversityList::find()->asArray()->all();
        $list = [];
        foreach ($query as $university){
            $list[$university['code']] = $university['name'];
        }
        return $list;
    }
}
