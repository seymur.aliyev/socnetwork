<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "city_list".
 *
 * @property int $id
 * @property string $name
 */
class CityList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }


    public function getCityList(){
        $query = CityList::find()->asArray()->all();
        $list = [];
        foreach ($query as $city){
            $list[$city['id']] = $city['name'];
        }
        return $list;
    }
}
