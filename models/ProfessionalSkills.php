<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "professional_skills".
 *
 * @property int $id
 * @property string $skill
 */
class ProfessionalSkills extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'professional_skills';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_id'], 'integer'],
            [['skill'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'skill' => Yii::t('app', 'Skill')
        ];
    }


    /**
     * @param $user_id
     * @param $skills
     * @return int
     * Saves logged user professional skills
     */
    public function saveUserSkills($user_id, $skills){
        $sql = "INSERT INTO professional_skills
                (`skill`,`user_id`) VALUES ";
                $values = '';
                foreach ($skills as $skill) {
                    $values .= "
                    (
                          '{$skill}',
                          '{$user_id}'
                    ),";
                }
        $values = rtrim($values,',');
        $query  = $sql.$values;

        return Yii::$app
            ->db
            ->createCommand($query)
            ->execute();
    }


    public function getAllSKills($user_id){
        $where = $params = [];
        $where[] = "ps.user_id = :user_id ";
        $params['user_id'] = $user_id;

        $sql = 'SELECT ps.skill 
                FROM professional_skills ps
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        $array =  Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryAll();

        $skills = [];
        foreach ($array as $k => $v) {
            $skills[$k] = $v['skill'];
        }
        return $skills;
    }
}
