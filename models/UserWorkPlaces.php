<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_work_places".
 *
 * @property int $id
 * @property int $user_id
 * @property string $company
 * @property string $position
 * @property string $city
 * @property string $start_date
 * @property string $over_date
 * @property string $currently_work
 */
class UserWorkPlaces extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_work_places';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            /*[['user_id'], 'integer'],
            [['company', 'position', 'city'], 'string', 'max' => 255],*/
            [['city','company','position','start_date'],'required'],
            [['company', 'position', 'city'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'company' => Yii::t('app', 'Şirkət'),
            'position' => Yii::t('app', 'Vəzifə'),
            'city' => Yii::t('app', 'Şəhər'),
            'start_date' => Yii::t('app', 'Başlama tarixi'),
            'over_date' => Yii::t('app', 'Bitmə tarixi'),
        ];
    }

    public function saveUserWorkPlace($userWorkData){
        $userWorkPlace = new UserWorkPlaces();
        $userWorkPlace->user_id = Yii::$app->user->identity['id'];
        $userWorkPlace->company = $userWorkData['company'];
        $userWorkPlace->city = $userWorkData['city'];
        $userWorkPlace->position = $userWorkData['position'];
        $userWorkPlace->start_date = $userWorkData['start_date'];
        $userWorkPlace->over_date = $userWorkData['over_date'];
        $userWorkPlace->currently_work = $userWorkData['currently_work'];
        $userWorkPlace->save();
    }

    public function allWorkPlaces($user_id){
        $where = $params = [];
        $where[] = "wp.user_id = :user_id ";
        $params['user_id'] = $user_id;

        $sql = 'SELECT wp.* 
                FROM user_work_places wp
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryAll();
    }

    public function getCurrentWorkPlace($user_id){
        $where = $params = [];
        $where[] = "wp.user_id = :user_id ";
        $where[] = "wp.currently_work = '1'";
        $params['user_id'] = $user_id;

        $sql = 'SELECT wp.* 
                FROM user_work_places wp
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryOne();
    }


    public function removeWorkPlace($id){
        $where = $params = [];
        $where[] = "id = :id ";
        $params[':id'] = $id;

        $sql = 'DELETE 
                FROM user_work_places
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->execute();
    }
}
