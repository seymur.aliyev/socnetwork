<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "friend".
 *
 * @property int $id
 * @property int $user_id
 * @property int $friend_id
 */
class Friend extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'friend';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['user_id', 'friend_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'friend_id' => Yii::t('app', 'Friend ID'),
        ];
    }


    public function getAllFriends($user_id){
        $where = $params = [];
        $where[] = "f.user_id= :user_id";
        $where[] = "f.request_sent = '1'";
        $where[] = "f.request_accepted = '1'";
        $where[] = "up.is_profile = '1'";

        $params[':user_id'] = $user_id;
        $sql = 'SELECT u.*,
                up.hash_name as hash_name
                FROM friend f
                LEFT JOIN UserTable u ON f.friend_id=u.id
                LEFT JOIN user_photos up ON up.user_id=f.friend_id
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        return Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryAll();
    }


    public function sendFriendRequest($user_id,$friend_id){
        $sql = "INSERT INTO friend (`user_id`,`friend_id`,`request_sent`) 
                VALUES('{$user_id}','{$friend_id}','1');
                ";
        return Yii::$app
            ->db
            ->createCommand($sql)
            ->execute();
    }


    public function getFriendshipStatusForUser($logged_user_id,$user_id){
        $where = $params = [];
        $where[] = "f.user_id = :logged_user_id";
        $params[':logged_user_id'] = $logged_user_id;
        $where[] = "f.friend_id = :user_id";
        $params[':user_id'] = $user_id;
        $sql = 'SELECT f.*
                FROM friend f
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        $result =  Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryOne();

        $output = '';
        if($result['request_sent'] == 1 && $result['request_accepted'] == '0'){
            $output = 'Friend request sent';
        }
        elseif($result['request_accepted'] == 1){
            $output = 'Friends';
        }
        elseif(!$result){
            $output = 'Add friend';
        }
        return $output;
    }


    public static function friendRequest($user_id){
        $where = $params = [];
        $where[] = "f.request_sent = '1'";
        $where[] = "f.request_accepted = '0'";
        $where[] = "f.friend_id = :user_id";
        $where[] = "up.is_profile = '1'";
        $params[':user_id'] = $user_id;

        $sql = 'SELECT u.name as name,
                up.*,
                f.id as friend_request_id
                FROM friend f
                LEFT JOIN UserTable u    ON u.id=f.user_id
                LEFT JOIN user_photos up ON up.user_id=f.user_id
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        $result =  Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryAll();

        return $result;
    }


    public function acceptFriendRequest($data){
        $accept = "UPDATE friend SET request_accepted = '1'
                WHERE id = '{$data['friend_request_id']}'";
        Yii::$app
            ->db
            ->createCommand($accept)
            ->execute();

        $sql = "INSERT INTO friend (`user_id`,`friend_id`,`request_sent`,`request_accepted`)
                VALUES('{$data['logged_user_id']}','{$data['friend_request_user_id']}','1','1');
                ";
        return Yii::$app
            ->db
            ->createCommand($sql)
            ->execute();
    }


    public function friendsOfFriend($logged_user_id,$friendIds){
        $where = $params = [];
        $where[] = "f.user_id IN (".implode(",",$friendIds).")";
        $where[] = "f.friend_id!=:logged_user_id";
        $where[] = "f.friend_id NOT IN (".implode(",",$friendIds).")";
        $where[] = "up.is_profile = '1'";
        $params[':logged_user_id'] = $logged_user_id;

        $sql = 'SELECT DISTINCT u.id as id,
                u.name as name,
                up.hash_name as hash_name
                FROM friend f
                LEFT JOIN UserTable u    ON u.id=f.friend_id
                LEFT JOIN user_photos up ON up.user_id=f.friend_id
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        $result = Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->queryAll();

        return $result;
    }

    public function unfriend($user_id, $friend_id){
        $where = $params = [];
        $where[] = "user_id=:user_id";
        $where[] = "friend_id=:friend_id OR
        (user_id=:friend_id AND friend_id=:user_id)";


        $params[':user_id'] = $user_id;
        $params[':friend_id'] = $friend_id;

        $sql = 'DELETE FROM friend
                '.(!empty($where) ? ' WHERE ' . implode(' AND ', $where) : '').'
                ';

        $result = Yii::$app
            ->db
            ->createCommand($sql,$params)
            ->execute();
        return $result;
    }
}
